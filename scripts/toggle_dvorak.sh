#! /usr/bin/env bash
#setxkbmap -query | grep dvorak > /dev/null && setxkbmap se || setxkbmap dvorak se

if setxkbmap -query | grep dvorak > /dev/null ; then
    setxkbmap se
    notify-send -t 2000 -a "Layout changed" "             QWERTY"
else
    setxkbmap dvorak se
    notify-send -t 2000 -a "Layout changed" "             DVORAK"
fi

