#! /usr/bin/env bash

filter_regex="vcc_ihu_tuner|obj"

paths=('/ext/ihu/'
       '/ext/sem/')

for path in "${paths[@]}"
do
    add_tags() { find $1 -name '*.h' -o -name '*.cpp' -o -name '*.hpp' -o -name '*.cc' -o -name '*.c' -o -name '*.hal' | egrep -v $filter_regex | xargs /run/current-system/sw/bin/ctags -e -a; }

    add_cscope() { find $1 -name '*.h' -o -name '*.cpp' -o -name '*.hpp' -o -name '*.cc' -o -name '*.c' -o -name '*.hal' | egrep -v $filter_regex >> cscope.files; }

    cd $path

    rm TAGS
    rm cscope.*
    touch csope.files
    tag_dirs=('vendor/aptiv/components'
              'vendor/aptiv/interfaces'
              'out/soong/.intermediates'
              'out/target/product'
              'system/libhidl'
              'system/core/libutils'
              'frameworks/av/media'
              'frameworks/av/services'
              'external/protobuf'
              'external/googletest')

    for i in "${tag_dirs[@]}"
    do
        add_tags $i
        add_cscope $i
    done
    cscope -b -q -k
done

