#! /usr/bin/env python3
import sys
import os
import re

def main():
    print(sys.argv[1])
    build_file = sys.argv[1]
    value = 0
    modules = list()
    if os.path.isfile(build_file):
        with open(build_file, 'r') as bfile:
            for line in bfile:
                line
                m = re.match(".*Install: out/target/product/ihu_abl_car/(.*)",line, re.I)
                if m:
                    module_name = line.split("/")[-1]
                    module_name = module_name.strip()
                    if module_name[-3:] == ".so":
                        module_name = module_name[:-3]
                    
                    modules.append(module_name)
                    value = value + 1

    build_string = "m"
    for module in modules:
        build_string = build_string + " clean-" + module

    print(build_string)


if __name__ == "__main__":
    main()



#todo only in emacs
####my_list = list()
#my_list.append("") my_list.append("/home/robban/tmp/build3.log")
#main(my_list)





