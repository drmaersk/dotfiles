(setq warning-minimum-level :emergency)
(package-initialize)
(let ((default-directory  "~/.emacs.d/"))
      (normal-top-level-add-subdirs-to-load-path))

(load "~/dotfiles/emacs/001-package.el")

;;(load "/home/robban/.emacs.d/elpa/cquery-20180413.938/cquery.el")

;;(load "~/dotfiles/emacs/color-theme-settings.el")
;;(load "~/dotfiles/emacs/line-endings.el")
(load "~/dotfiles/emacs/helm.el")
(load "~/dotfiles/emacs/ibuffer.el")
(load "~/dotfiles/emacs/editor.el")
(load "~/dotfiles/emacs/python.el")
(load "~/dotfiles/emacs/locate.el")
(load "~/dotfiles/emacs/scratch.el")
(load "~/dotfiles/emacs/ccls.el")
(load "~/dotfiles/emacs/hydra.el")
(load "~/dotfiles/emacs/doom.el")
;;(load "~/dotfiles/emacs/grok.el")
(load "~/dotfiles/emacs/avy.el")
(load "~/dotfiles/emacs/cscope.el")
(load "~/dotfiles/emacs/nix-mode.el")
;;(load "~/dotfiles/emacs/gitgutter.el") bugging
(load "~/dotfiles/emacs/magit.el")
;; Keep last so everything is loaded
(load "~/dotfiles/emacs/xah.el")
(load "~/dotfiles/emacs/keybindings.el")
;; Package: dtrt-indent
(require 'dtrt-indent)
(dtrt-indent-mode 1)

;; Package: ws-butler
(require 'ws-butler)
(add-hook 'c-mode-common-hook 'ws-butler-mode)

(which-key-mode)

;; No backup-files
(setq backup-directory-alist `(("." . "~/.saves")))

(setq auto-save-default nil) ; stop creating #autosave# files
(setq make-backup-files nil)
(setq delete-old-versions  t)
(setq x-select-enable-clipboard t)

;; Plant-uml stuff
;; (require 'flycheck-plantuml)
;; (flycheck-plantuml-setup)
;; (add-to-list 'auto-mode-alist '("\\.uml\\'" . plantuml-mode))
;; (add-hook 'plantuml-mode-hook 'flycheck-mode)

(use-package better-defaults
  :ensure t
  )

(use-package string-inflection
  :ensure t
  )

(use-package cmake-mode
  :ensure t
  )

(use-package plantuml-mode
  :ensure t
  )

(use-package yasnippet
  :ensure t
  :config
  (yas-global-mode 1)
  )

(use-package markdown-mode
  :ensure t
  :mode ("\\.md\\'" . markdown-mode))

(require 'package)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.milkbox.net/packages/") t)
(package-initialize)

(setq yas-snippet-dirs
      '("~/.emacs.d/snippets"                 ;; personal snippets
        "~/.emacs.d/elpa/yasnippet-snippets-0.11/snippets/"
        ))

(setq-default flycheck-disabled-checkers '(c/c++-clang c/c++-cppcheck c/c++-gcc))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(flycheck-error ((t (:background "red" :foreground "white" :box nil :underline (:color "white" :style wave) :slant italic)))))


(ws-butler-global-mode)

(setq-default indent-tabs-mode nil)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-backends
   (quote
    (company-lsp company-xcode company-cmake company-capf company-files
                 (company-dabbrev-code company-gtags company-etags company-keywords)
                 company-oddmuse company-dabbrev)))
 '(helm-locate-command
   "locate %s -d /home/robban/.config/locatedb/ihudb -e -A --regex %s")
 '(lsp-ui-sideline-enable nil)
 '(package-selected-packages
   (quote
    (yasnippet-snippets yasnippet cmake-mode string-inflection better-defaults helm-xref magit-gerrit doom-themes helm-cscope ripgrep counsel-etags avy json-mode elpy guide-key smart-mode-line cquery ws-butler dtrt-indent helm-ag flycheck-irony flycheck flymd ibuffer-projectile helm-gtags ggtags helm company))))

(setq c-default-style "bsd"
  c-basic-offset 4)
