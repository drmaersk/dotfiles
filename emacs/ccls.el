

;;ccls
(use-package helm-xref
  :ensure t
  :config
  (setq xref-show-xrefs-function 'helm-xref-show-xrefs)
  )

(use-package lsp-mode :commands lsp)
(use-package lsp-ui :commands lsp-ui-mode)
(use-package company-lsp :commands company-lsp)

(use-package ccls
  :hook ((c-mode c++-mode objc-mode) .
         (lambda () (require 'ccls) (lsp))))

(setq ccls-executable "/run/current-system/sw/bin/ccls")

(with-eval-after-load 'projectile
  (setq projectile-project-root-files-top-down-recurring
        (append '("compile_commands.json"
                  ".ccls")
                projectile-project-root-files-top-down-recurring)))



(defun ccls//enable ()
  (condition-case nil
      (progn
        (lsp-ccls-enable)
        (lsp-ui-mode)
        (company-mode)
        (yas-minor-mode-on)
        (setq lsp-ui-sideline-mode nil)
        (setq lsp-ui-peek-fontify 'always)
        )
    (user-error nil)))

(defun ccls/callee ()
  (interactive)
  (lsp-ui-peek-find-custom 'callee "$ccls/call" '(:callee t)))
(defun ccls/caller ()
  (interactive)
  (lsp-ui-peek-find-custom 'caller "$ccls/call"))
(defun ccls/vars (kind)
  (lsp-ui-peek-find-custom 'vars "$ccls/vars" `(:kind ,kind)))
(defun ccls/base (levels)
  (lsp-ui-peek-find-custom 'base "$ccls/inheritance" `(:levels ,levels)))
(defun ccls/derived (levels)
  (lsp-ui-peek-find-custom 'derived "$ccls/inheritance" `(:levels ,levels :derived t)))
(defun ccls/member (kind)
  (lsp-ui-peek-find-custom 'member "$ccls/member" `(:kind ,kind)))

;; The meaning of :role corresponds to https://github.com/maskray/ccls/blob/master/src/symbol.h

;; References w/ Role::Address bit (e.g. variables explicitly being taken addresses)
(defun ccls/references-address ()
  (interactive)
  (lsp-ui-peek-find-custom
   'address "textDocument/references"
   (plist-put (lsp--text-document-position-params) :context
              '(:role 128))))

;; References w/ Role::Dynamic bit (macro expansions)
(defun ccls/references-macro ()
  (interactive)
  (lsp-ui-peek-find-custom
   'address "textDocument/references"
   (plist-put (lsp--text-document-position-params) :context
              '(:role 64))))

;; References w/o Role::Call bit (e.g. where functions are taken addresses)
(defun ccls/references-not-call ()
  (interactive)
  (lsp-ui-peek-find-custom
   'address "textDocument/references"
   (plist-put (lsp--text-document-position-params) :context
              '(:excludeRole 32))))

;; References w/ Role::Read
(defun ccls/references-read ()
  (interactive)
  (lsp-ui-peek-find-custom
   'read "textDocument/references"
   (plist-put (lsp--text-document-position-params) :context
              '(:role 8))))

;; References w/ Role::Write
(defun ccls/references-write ()
  (interactive)
  (lsp-ui-peek-find-custom
   'write "textDocument/references"
   (plist-put (lsp--text-document-position-params) :context
              '(:role 16))))


(defun my/highlight-pattern-in-text (pattern line)
  (when (> (length pattern) 0)
    (let ((i 0))
     (while (string-match pattern line i)
       (setq i (match-end 0))
       (add-face-text-property (match-beginning 0) (match-end 0) 'isearch t line)
       )
     line)))

(with-eval-after-load 'lsp-methods
  ;;; Override
  ;; This deviated from the original in that it highlights pattern appeared in symbol
  (defun lsp--symbol-information-to-xref (pattern symbol)
   "Return a `xref-item' from SYMBOL information."
   (let* ((location (gethash "location" symbol))
          (uri (gethash "uri" location))
          (range (gethash "range" location))
          (start (gethash "start" range))
          (name (gethash "name" symbol)))
     (xref-make (format "[%s] %s"
                        (alist-get (gethash "kind" symbol) lsp--symbol-kind)
                        (my/highlight-pattern-in-text (regexp-quote pattern) name))
                (xref-make-file-location (string-remove-prefix "file://" uri)
                                         (1+ (gethash "line" start))
                                         (gethash "character" start)))))

  (cl-defmethod xref-backend-apropos ((_backend (eql xref-lsp)) pattern)
    (let ((symbols (lsp--send-request (lsp--make-request
                                       "workspace/symbol"
                                       `(:query ,pattern)))))
      (mapcar (lambda (x) (lsp--symbol-information-to-xref pattern x)) symbols)))
  )


(use-package ccls
  :commands lsp-ccls-enable
  :init
  (add-hook 'c-mode-hook #'ccls//enable)
  (add-hook 'c++-mode-hook #'ccls//enable)
  :config
  (setq ccls-sem-highlight-method 'font-lock)
;; alternatively, (setq ccls-sem-highlight-method 'overlay)
;; For rainbow semantic highlighting
;;  (ccls-use-default-rainbow-sem-highlight)
  )

  (use-package company
    :ensure t
    :config
    (setq company-idle-delay 2.5)
    (setq company-tooltip-limit 10)
    (setq company-minimum-prefix-length 2)
    ;; invert the navigation direction if the the completion popup-isearch-match
    ;; is displayed on top (happens near the bottom of windows)
    (setq company-global-modes '(not org-mode text-mode emacs-lisp-mode))
    (global-company-mode 1)
    )

(setq company-transformers nil company-lsp-async t company-lsp-cache-candidates nil)

;;(setq ccls-extra-init-params '(:cacheDirectory "/home/robban/.config/ccls" :enableComments 2 :cacheFormat "msgpack" :completion (:detailedLabel t)  ))

;; ;; Log file
(setq ccls-extra-args '("--log-file=/home/robban/.config/ccls/cq.log"))
;; ;; Cache directory, both relative and absolute paths are supported
(setq ccls-cache-dir "/home/robban/.config/ccls/")
;; ;; Initialization options
(setq ccls-extra-init-params '(:cacheDirectory ("/home/robban/.config/ccls")
                                               :cacheFormat ("json")
                                               :completion (:detailedLabel t)
                                               :index (:threads 4)
                                               :clang (:extraArgs ("-ferror-limit=0"
                                                   ;;                 "-std=gnu++14"
                                                   ;;                 "-fexceptions"
                                                   ;; "-isystem /home/robban/dev/ihu/system/core/base/include"
                                                   ;; "-isystem /home/robban/dev/ihu/bionic/libm/include/amd64"
                                                   ;; "-isystem /home/robban/dev/ihu/bionic/libm/include"
                                                   ;; "-isystem /home/robban/dev/ihu/bionic/libc/kernel/uapi/asm-x86"
                                                   ;; "-isystem /home/robban/dev/ihu/bionic/libc/kernel/common"
                                                   ;; "-isystem /home/robban/dev/ihu/bionic/libc/kernel/android/uapi"
                                                   ;; "-isystem /home/robban/dev/ihu/bionic/libc/kernel/uapi"
                                                   ;; "-isystem /home/robban/dev/ihu/bionic/libc/include"
                                                   ;; "-isystem /home/robban/dev/ihu/bionic/libc/arch-x86_64/include"
                                                   ;; "-isystem /home/robban/dev/ihu/device/intel/common/kernel/bxt/kernel-headers"
                                                   ;; "-isystem /home/robban/dev/ihu/out/target/product/$TARGET_NAME/obj/include"
                                                   ;; "-isystem /home/robban/dev/ihu/frameworks/base/include"
                                                   ;; "-isystem /home/robban/dev/ihu/frameworks/av/include"
                                                   ;; "-isystem /home/robban/dev/ihu/frameworks/native/opengl/include"
                                                   ;; "-isystem /home/robban/dev/ihu/frameworks/native/include"
                                                   ;; "-isystem /home/robban/dev/ihu/libnativehelper/include"
                                                   ;; "-isystem /home/robban/dev/ihu/hardware/ril/include"
                                                   ;; "-isystem /home/robban/dev/ihu/hardware/libhardware_legacy/include"
                                                   ;; "-isystem /home/robban/dev/ihu/hardware/libhardware/include"
                                                   ;; "-isystem /home/robban/dev/ihu/system/media/audio/include"
                                                   ;; "-isystem /home/robban/dev/ihu/system/core/include"
                                                   ;; "-isystem /home/robban/dev/ihu/external/googletest/googlemock/include"
                                                   ;; "-isystem /home/robban/dev/ihu/external/googletest/googletest/include"
                                                   ;; "-isystem /home/robban/dev/ihu/external/protobuf/include"
                                                   ;; "-isystem /home/robban/dev/ihu/external/libcxxabi/include"
                                                   ;; "-isystem /home/robban/dev/ihu/external/libcxx/include"
                                                   ;; "-isystem /home/robban/dev/ihu/libnativehelper/include/nativehelper"
                                                   ;; "-isystem /home/robban/dev/ihu/external/protobuf/src"
                                                   )
                                                                  :clang (:excludeArgs ("-Wall"
                                                                                        "-Werror"
                                                                                        "-Wconversion"
                                                                     "--system-header-prefix")))))



;; Value: ((-32700 "Parse Error")
;;  (-32600 "Invalid Request")
;;  (-32601 "Method not Found")
;;  (-32602 "Invalid Parameters")
;;  (-32603 "Internal Error")
;;  (-32099 "Server Start Error")
;;  (-32000 "Server End Error")
;;  (-32002 "Server Not Initialized")
;;  (-32001 "Unknown Error Code")
;;  (-32800 "Request Cancelled"))

(push '-32603 lsp--silent-errors) ;;Internal Error"
(push '-32002 lsp--silent-errors) ;;Server Not Initialized"
(push '-32001 lsp--silent-errors) ;;Unknown Error Code"
(push '-32700 lsp--silent-errors) ;; "Parse Error
(push '-32601 lsp--silent-errors) ;; Method not Found
(push '-32600 lsp--silent-errors) ;; Invalid Request




