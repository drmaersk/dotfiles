(use-package clang-format
  :ensure t
  :init (fset 'c-indent-region 'clang-format-region)
        (fset 'c-indent-line-or-region 'clang-format-region)
  :bind ("C-c C-i" . clang-format-buffer))


