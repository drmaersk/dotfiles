(use-package hydra
  :ensure t)
  ;; no :pin needed, as package.el will choose the version in melpa


;; (defhydra hydra-zoom (global-map "<f2>")
;;   "zoom"
;;   ("g" text-scale-increase "in")
;;   ("l" text-scale-decrease "out"))

(defhydra hydra-super-gitter (:color pink
                             :hint nil)

  "

^  ^Commands^        
^  ^^^^^^^^----------
^  _s_: status   
^  _g_: push HEAD to gerrit
^  ^ ^
"
  ("s" magit-status :exit t)
  ("g" magit-push-to-gerrit :exit t)
  ("q" nil "quit"))

(global-set-key (kbd "C-x s") 'hydra-super-gitter/body)

(defhydra hydra-super-searcher (:color pink
                             :hint nil)

  "

^  ^Find^             ^Show^             ^Actions^          ^Search
^  ^^^^^^^^-----------------------------------------------------------------
^  _r_: references    _G_: GIT           _p_: preprocess    _f_: file
^  _d_: definition    _c_: callers       _R_: rename        _P_: replace
^  _a_: apropos       _m_: members       ^ ^                _O_: multi-occur
^  _g_: grep          ^ ^                ^ ^
^  ^ ^
"
  ("r" lsp-ui-peek-find-references :exit t)
  ("d" lsp-ui-peek-find-definitions :exit t)
  ("a" xref-find-apropos :exit t)
  ("g" robb-grep :exit t)
  ("m" ccls-member-hierarchy :exit t)
  ("c" ccls-call-hierarchy :exit t)
  ("G" hydra-super-gitter/body :exit t)
  ("R" lsp-rename :exit t)
  ("p" ccls-preprocess-file :exit t)
  ("O" Buffer-menu-multi-occur :exit t)
  ("P" query-replace :exit t)
  ("f" robb-search-in-file :exit t)
  ("q" nil "quit"))
 
(global-set-key [f2] 'hydra-super-searcher/body)

(defhydra hydra-locater (:color pink
                             :hint nil)

  "

^  ^Commands^        
^  ^^^^^^^^----------
^  _1_: rootdb
^  _2_: ihudb
^  _3_: ihu_aptivdb
^  _4_: semdb
^  _5_: sem_aptivdb
^  _s_: search
^  ^ ^
"
  ("1" setlocate-root)
  ("2" setlocate-ihu)
  ("3" setlocate-ihu_aptiv)
  ("4" setlocate-sem)
  ("5" setlocate-sem_aptiv)
  ("s" helm-locate :exit t))
(global-set-key (kbd "C-l") 'hydra-locater/body)


(setq gdb-display-io-nopopup t)
(defhydra hydra-debug (:color pink
                             :hint nil)
"

^  ^Commands^        
^  ^^^^^^^^----------
^  _u_: run
^  _c_: cont
^  _a_: add break
^  _r_: remove break
^  _s_: stepin
^  _n_: step
^  _l_: show locals
^  _w_: watch
^  _b_: show breakpoints
^  _m_: show memory
^  ^ ^
"
  ("u" gud-run)
  ("c" gud-cont)
  ("a" gud-break)
  ("r" gud-remove)
  ("s" gud-step)
  ("n" gud-next)
  ("l" gdb-frame-locals-buffer)
  ("w" gud-watch)
  ("b" gdb-frame-breakpoints-buffer)
  ("m" gdb-display-memory-buffer)
  ("q" nil "quit"))
(global-set-key (kbd "C-e") 'hydra-debug/body)


(defun jumpandkill () (interactive)
       (progn
         (let ((register (read-key "Enter register: ")))
           (switch-to-buffer-other-window "*Output*")
           (delete-window)
           (jump-to-register register))
         ))
;;

(defhydra hydra-bookmark (:color pink
                             :hint nil)
"
^  ^Commands^
^  ^^^^^^^^----------
^  _m_: bookmark file
^  _l_: list bookmarks
^  _p_: point to register
^  _r_: list register
^  _j_: jump to register
^  ^ ^
"
  ("m" bookmark-set :exit t)
  ("l" bookmark-bmenu-list :exit t)
  ("p" point-to-register :exit t)
  ("r" helm-register :exit t)
  ("j" jump-to-register :exit t)
  ("q" nil "quit"))
(global-set-key (kbd "C-b") 'hydra-bookmark/body)
