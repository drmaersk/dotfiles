;; IBUFFER
(require 'ibuffer)
(require 'ibuffer-projectile)
;; (setq ibuffer-saved-filter-groups
;;       (quote (("default"
;; 	       ("test"
;; 		(filename . "\.\*test/"))
;;                ("intel" (filename .   "\.\*vendor\.intel"))
;; 	       ("vcc_ihu" (filename .   "\.\*delphi/vcc_ihu"))
;; 	       ("delphi" (filename .   "\.\*delphi"))
;; 	       ("Code"
;; 	       		(or (mode . c-mode)
;; 	       		    (mode . c++-mode)))
;; 	       ("Makefiles" (or
;; 			     (name . "^Makefile\.\*")
;; 			     (name . "^CMakeLists\\.txt\.\*")))
;; 	       ("Directories" (mode . dired-mode))
;; 	       ("Symrefs" (or
;; 			   (name . "^\\*cscope\\*$")
;; 			   (name . "^TAGS\.\*")))

;; 	       ))))

;; ;;(add-to-list 'ibuffer-never-show-regexps "^\\*")
;; (require 'ibuf-ext)
;; (add-to-list 'ibuffer-never-show-predicates "^\\*")
;;  ;;hide groups with no buffers
;; (setq ibuffer-show-empty-filter-groups nil)

;;     (add-hook 'ibuffer-mode-hook
;;               (lambda ()
;;                 (ibuffer-switch-to-saved-filter-groups "default")))

;; (setq ibuffer-formats
;;       '( (mark modified read-only " " (name 35 35 :left :elide) " " (filename 9 -1 :right))))


(define-ibuffer-column size-h
  (:name "Size" :inline t)
  (cond
   ((> (buffer-size) 1000000) (format "%7.1fM" (/ (buffer-size) 1000000.0)))
   ((> (buffer-size) 100000) (format "%7.0fk" (/ (buffer-size) 1000.0)))
   ((> (buffer-size) 1000) (format "%7.1fk" (/ (buffer-size) 1000.0)))
   (t (format "%8d" (buffer-size)))))

;; Modify the default ibuffer-formats
  (setq ibuffer-formats
	'((mark modified read-only " "
;;                (mode 16 16 :left :elide)
		" "
                (name 32 32 :left :elide)
		" "
		filename-and-process)))



(add-hook 'ibuffer-hook
    (lambda ()
      (ibuffer-projectile-set-filter-groups)
      (unless (eq ibuffer-sorting-mode 'alphabetic)
        (ibuffer-do-sort-by-alphabetic))))
