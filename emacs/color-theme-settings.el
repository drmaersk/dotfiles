;; COLOR-THEME
(require 'color-theme)
(color-theme-initialize)
(color-theme-subtle-hacker)
;;(color-theme-calm-forest)
;;(color-theme-clarity)
;;(if (window-system) (set-frame-size (selected-frame) 85 55))
;;find other fonts with fc-list
;;(set-default-font "Bitstream Vera Sans Mono-10")
;;(set-default-font "Lucida Sans-10")
