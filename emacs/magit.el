(use-package magit-gerrit
  :init
  (setq-default magit-gerrit-ssh-creds "mj08y1@10.236.95.27")
  :ensure t)


(defun magit-push-to-gerrit ()
  (interactive)
(magit-git-command-topdir "git push origin HEAD:refs/for/master"))

(magit-define-popup-action 'magit-push-popup
  ?m
  "Push to gerrit"
  'magit-push-to-gerrit)
