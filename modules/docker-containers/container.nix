
{stdenv, name, dockersource, xhost}:

let
build = ''
#!/usr/bin/env bash

USER_ID="$(id -u "$USER")"
DIR="$( cd "$( dirname "''${BASH_SOURCE[0]}" )" && pwd )"

if [[ "$1" == "-c" ]]
then
  { cat << 'EOT'; cat << EOT; } | docker build --no-cache -t "${name}" -
${dockersource}
EOT
RUN useradd -r -s /bin/bash -u $USER_ID $USER -G plugdev
RUN echo $USER:$USER | chpasswd && adduser $USER sudo
RUN groupadd adbusers
RUN usermod -a -G adbusers $USER
RUN usermod -a -G users $USER
USER $USER
EOT
else
  { cat << 'EOT'; cat << EOT; } | docker build -t "${name}" -
${dockersource}
EOT
RUN useradd -r -s /bin/bash -u $USER_ID $USER -G plugdev 
RUN echo $USER:$USER | chpasswd && adduser $USER sudo 
RUN groupadd adbusers
RUN usermod -a -G adbusers $USER
RUN usermod -a -G users $USER
USER $USER
EOT
fi
'';

run = ''
#!/usr/bin/env bash

source /home/robban/.config/docker_aliases

USER_ID="$(id -u "$USER")"
${xhost}/bin/xhost +si:localuser:$USER

# To setup ccache from AOSP run prebuilts/misc/linux-x86/ccache/ccache -M 100G
docker run \
  -e DISPLAY=$DISPLAY \
  -v /tmp/.X11-unix:/tmp/.X11-unix:ro \
  --privileged \
  -v /dev/bus/usb:/dev/bus/usb \
  --device /dev/kvm \
  -u "$USER_ID" \
  -e HOME=$HOME \
  -e USER=$USER \
  -e USE_CCACHE=1 \
  -e CCACHE_DIR=/ext/.ccache \
  -e CC_WRAPPER=/usr/bin/ccache \
  -e CXX_WRAPPER=/usr/bin/ccache \
  -v $HOME/:$HOME \
  -v /ext/:/ext \
  -w "$PWD" \
  --net=host \
  --env-file /home/robban/.config/docker_aliases \
  -it "${name}"
'';

in
stdenv.mkDerivation {
  name = name;

  buildInputs = [ xhost ];

  buildCommand = ''
    mkdir -p $out/bin
    cat << 'EOF' > $out/bin/build-${name}
${build}
EOF
    chmod +x $out/bin/build-${name}

    cat << 'EOF' > $out/bin/${name}
${run}
EOF
    chmod +x $out/bin/${name}
  '';
}
