{ config, lib, pkgs, ... }:

{
  imports = [ ];

  environment.systemPackages = with pkgs; [
    cntlm
  ];
  services.cntlm.enable = false;
  services.cntlm.domain = "europe";
  services.cntlm.username = "mj08y1";
  services.cntlm.proxy = [ "proxyrrnl.delphiauto.net:8080"
                           "proxynlvip.delphiauto.net:8081"];
  services.cntlm.password = "";
  services.cntlm.extraConfig = "
NoProxy localhost, 127.0.0.*, 10.*, 192.168.*, *.delphiauto.net  
";

  #switch to this to disable proxy
  networking.proxy.noProxy = "*";
  networking.proxy.default = "http://127.0.0.1:3128";

  environment.etc.gitconfig.text = ''
  [url "https://github.com/"]
    insteadOf = git://github.com/
	[url "https://github.com/openembedded/"]
    insteadOf = git://git.openembedded.org/
	[url "https://git.yoctoproject.org/git/"]
    insteadOf = git://git.yoctoproject.org/
	[url "https://git.kernel.org/"]
    insteadOf = git://git.kernel.org/
	[url "http://sourceware.org/git/"]
    insteadOf = git://sourceware.org/git/
	[url "http://code.qt.io/"]
    insteadOf = git://code.qt.io/
	[url "http://git.projects.genivi.org/"]
    insteadOf = git://git.projects.genivi.org/
	[url "http://anongit.freedesktop.org/git/"]
    insteadOf = git://anongit.freedesktop.org/
  '';
}
