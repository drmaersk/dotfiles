# default.nix
with import <nixpkgs> {};
let
gcc = "${pkgs.gcc6}";
in
stdenv.mkDerivation rec {
    name = "dev-environment"; # Probably put a more meaningful name here
    buildInputs = [ pkgconfig
cmake
gcc
python2
python3
utillinux
glibc_multi
libxmlxx
zlib
jre
protobuf3_1
git
less
ps
];
  # The ${...} syntax is string interpolation in Nix
  shellHook = ''
    echo "audio-build"
    export CC="${gcc}/bin/gcc"
    export CXX="${gcc}/bin/g++"
    export LD_LIBRARY_PATH="/var/run/current-system/sw/lib"
  '';
}


