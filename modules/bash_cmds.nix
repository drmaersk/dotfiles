{ pkgs, config, ... }:
let
  ##############################################
  redshift_cmd = pkgs.writeScriptBin "f.lux" 
  ''
      ${pkgs.coreutils}/bin/env ${pkgs.bash}/bin/bash redshift -l 55.7:12.6 -t 5700:3600 -g 0.8 -m randr -v
  '';

  ##############################################
  sethome_cmd =  pkgs.writeScriptBin "home" 
  ''
  #!${pkgs.bash}/bin/bash
  xrandr --dpi 160;
  xset s off -dpms;
  ${(builtins.readFile /home/robban/.screenlayout/home.sh)}
  randomwall
  '';

  ##############################################
  # gpg -d --batch --passphrase gargamel .auth.gpg 2> /dev/null 1> /tmp/tmp.t
  #python3 -c "import base64; print(base64.b64encode(b'data to be encoded').decode('utf-8'))"
  setwork_cmd = pkgs.writeScriptBin "work" 
  ''
      #!${pkgs.bash}/bin/bash
      . /home/robban/.private/.auth
      ${(builtins.readFile /home/robban/dev/scripts/start_tmux)}
      ${(builtins.readFile /home/robban/.screenlayout/work2.sh)}
      pidgin
      randomwall
  '';

  ##############################################
  rpaper_cmd = pkgs.writeScriptBin "randomwall" 
    ''
      paper=`ls /home/robban/Downloads/wallpapers |sort -R |tail -1`
      feh --bg-fill /home/robban/Downloads/wallpapers/$paper
      '';
  ##############################################
  proxy_d_cmd = pkgs.writeScriptBin "proxy_disable" 
    ''
      paper=`ls /home/robban/Downloads/wallpapers |sort -R |tail -1`
      feh --bg-fill /home/robban/Downloads/wallpapers/$paper
      '';
  ##############################################
  proxy_e_cmd = pkgs.writeScriptBin "proxy_enable" 
    ''
      paper=`ls /home/robban/Downloads/wallpapers |sort -R |tail -1`
      feh --bg-fill /home/robban/Downloads/wallpapers/$paper
      '';
    ##############################################
  emacs_cmd = pkgs.writeScriptBin "xem" 
    ''
       emacsclient -c
      '';
  ##############################################
  connect_headphones_cmd =  pkgs.writeScriptBin "sony"
  ''
  #!${pkgs.bash}/bin/bash
  ${(builtins.readFile /home/robban/dev/scripts/connect_sony2.sh)}
  '';
  ##############################################
  notepad_pp_cmd =  pkgs.writeScriptBin "npp"
  ''
  #!${pkgs.bash}/bin/bash
  ${(builtins.readFile /home/robban/dev/scripts/npp.sh)}
  '';
      
in {
   environment.systemPackages = [
   redshift_cmd
   sethome_cmd
   setwork_cmd
   rpaper_cmd
   proxy_e_cmd
   proxy_d_cmd
   emacs_cmd
   connect_headphones_cmd
   notepad_pp_cmd];
}
