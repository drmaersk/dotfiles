with import <nixpkgs> {};
#{ stdenv, fetchFromGitHub, cmake, llvmPackages_6 }:
llvmPackages_6.stdenv.mkDerivation rec {
  name = "ccls-${version}";
  version = "20180925";
  src = fetchFromGitHub {
    owner = "MaskRay";
    repo = "ccls";
    rev = "41fcc0272c8c93fcbe52f24ef587e633a9dcc7e1";
    sha256 = "1w2dlg2ak03givxdngf4bqcirwnxnnv79jnljb6r4p53g5lfwc15";
    fetchSubmodules = true;
  };
  enableParallelBuilding = true;
  nativeBuildInputs = [ cmake ];
  buildInputs = [ llvmPackages_6.llvm llvmPackages_6.libclang
                  llvmPackages_6.libclang.out ];
  cmakeFlags = [
    "-DSYSTEM_CLANG=ON"
    "-DUSE_SHARED_LLVM=ON"
  ];

  meta = {
    description = "C/C++/ObjC language server";
    homepage = https://github.com/MaskRay/ccls;
    license = stdenv.lib.licenses.mit;
    platforms = stdenv.lib.platforms.all;
  };
}
