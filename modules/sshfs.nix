{ pkgs, config, ... }:
let

  ##############################################

   my_fuse_conf = pkgs.writeTextFile {
       name = "fuse.conf";
       text = "user_allow_other";
       destination = pkgs.sshfs/etc/fuse.conf;
     };

in {
   environment.systemPackages = [
       my_fuse_conf
       pkgs.sshfs
   ];
}
