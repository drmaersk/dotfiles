# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, nixpkgs, ... }:

let
  pidgin = pkgs.pidgin-with-plugins.override {
    plugins = [ pkgs.pidginsipe ];
  };

my-python-packages = python-packages: with python-packages; [
    requests
    pyqt5
    i3ipc
    xlib
    pexpect
    libtmux
    virtualenvwrapper
    tkinter
    python-daemon
    # other python packages you want
  ]; 
python-w-packages = pkgs.python3.withPackages my-python-packages;

svrofi = import ./modules/rofi/rofi.nix { inherit pkgs; };
urxvt = import ./modules/urxvt/urxvt.nix { inherit pkgs; };
ff-netflix = import ./modules/ff-netflix.nix {inherit pkgs; };
rename_ws = import ./modules/python_cmds.nix {inherit pkgs; };

in
{
  imports =
    [ # Include the results of the hardware scan.
    /etc/nixos/hardware-configuration.nix
#    ./modules/adb.nix
    ./modules/rice/rice.nix
    ./modules/bash_cmds.nix
    ./modules/docker.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.grub.useOSProber = true;
  boot.loader.grub.configurationLimit = 5;
  boot.loader.timeout = 3;

#sound stuff
#restart sound system
#pulseaudio -k && sudo alsa force-reload
  boot.extraModprobeConfig = ''
    options snd slots=snd-hda-intel
  '';

  sound.enable = true;	   
  nixpkgs.config.pulseaudio = true;
  hardware.pulseaudio = {
    enable = true;
    support32Bit = true;
    zeroconf.discovery.enable = true;
    package = pkgs.pulseaudioFull;
  };

  hardware.bluetooth.enable = true;
  hardware.bluetooth.extraConfig = "
  [General]
  Enable=Source,Sink,Media,Socket
";

  # Don't load pcspeaker module
  boot.blacklistedKernelModules = [ "snd_pcsp" ];

  # Set your time zone.
  time.timeZone = "Europe/Stockholm";

  environment.systemPackages = with pkgs; [
    #nix-repl
    audacity
    wget
   # emacs
    firefox
    gitRepo  #Android repo
    git
    pidgin
    chromium
    keepassx-community
    global
    kdiff3
    meld
    minicom
    exfat
    exfat-utils
    fuse_exfat
    jmtpfs  #android usb
    usbutils #android usb
  	adbfs-rootless #android usb
    discount
    #clipster #clipboard manager
    #autocutsel #clipboard manager
    #android-udev-rules #android usb
    clipit
    gptfdisk
    libreoffice
    dosfstools
    gdb
    fzf
    unzip
    zlib
    openssl
    rtags
    ntfs3g  
    pavucontrol
    lsof
    ag
    spotify
    parcellite
    jre
    graphviz
    doxygen
    tomcat8
    opengrok
    lm_sensors
    bluez
    bluez-tools
    blueman
    python-w-packages
    python2
    gdb
    cpulimit
    file
    libcgroup
    lcov
    vlc
    tmux
    xorg.xbacklight
    xorg.xwininfo
    xorg.xev
    xorg.xdpyinfo
    (import ./modules/vim.nix)
#    (import ./modules/ccls)
    ccls
    (import ./modules/delphi_vpn) 
    arandr
    networkmanagerapplet
    pnmixer
    urxvt
    chntpw #used for reading windows part
    libnotify
    p7zip
    evtest
    spotify
    rofi
    svrofi
    ranger
    feh
    qbittorrent
    mlocate
    dunst
    ff-netflix
    rename_ws
    xdotool
    redshift
    universal-ctags
    ripgrep
    solaar
    clang-tools
    cscope
    thunderbolt
    hdparm
    nixnote2
    epdfview
    rclone
    i3lock-fancy
    xorg.xmodmap
    wine
    pkgconfig
    playerctl
    cifs-utils
    sshfs
    pinta
    shutter
    emulationstation
    retroarch
    gnupg
  ];

#Qt environment
#nix-shell -p qtcreator qt5Full gnumake gcc gdb cmake

  programs.adb.enable = true;

  environment.variables.GRAPHVIZ_DOT="${pkgs.graphviz}/bin/dot";
  # environment.variables.TERM="xterm-256color"; gets overriden, workaround to get tmux script to work in shellinit
  environment.variables.EDITOR="emacsclient -c";
  environment.variables.ANDROID_BUILD_TOP="/ext/ihu";
  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  programs.bash.enableCompletion = true;

  programs.bash.interactiveShellInit = ''
    export TERM=xterm-256color
    title() { printf '\033]2;'$1'\a'; }
    add_tags() { find $1 -name '*.h' -o -name '*.cpp' -o -name '*.cc' -o -name '*.c' | xargs ctags -e -a; }
    compile_commands_convert_sem() { sed -i -e 's|/home/robban/sem|/ext/sem|g' compile_commands.json; }
    compile_commands_convert_ihu() { sed -i -e 's|/home/robban/ihu|/ext/ihu|g' compile_commands.json; }
    npp() { wine /home/robban/.wine/drive_c/npp/notepad++.exe $1 &> /dev/null & disown; }
    find_srcs() { find ./ -type f \( -iname \*.cpp -o -iname \*.h \); }
    search_replace() { arg="s|$1|$2|g";find_srcs | xargs sed -i -e $arg; }
    copy_all_files() { find $2 -name $1 | xargs -I % cp % $3; }
  '';
  
  programs.bash.shellAliases = {
    "rg"="rg --color=never";
    "gg" = "DISPLAY=:0 xdotool keyup super";
    "spk" = "speaker-test --force-frequency -f20 -t sine -p1000000";
    "less" = "less -i";
    "ll" = "ls -al";
    "ec" = "emacsclient --no-wait";
    "nixos-cleanup" = "sudo nix-collect-garbage -d";
    "emc" = "emacsclient --no-wait";
    "vbmeta"="/ext/ihu/external/avb/avbtool make_vbmeta_image --flag 2 --output vbmeta.img;echo 'done'";
    "sync"="adb root;adb remount;adb sync;adb shell sync";
    "build_server_connect"="ssh robban@10.239.124.56";
    "build_server_audio_connect"="ssh robban@10.239.126.22";
    "copy_build_server_compile_commands_ihu"="scp robban@10.239.124.56:/home/robban/ihu/compile_commands.json .";
    "copy_build_server_compile_commands_sem"="scp robban@10.239.124.56:/home/robban/sem/compile_commands.json .";
    "copy_build_server_ihu"="scp -r robban@10.239.124.56:/home/robban/ihu/out/target/product/ihu_abl_car/ihu_kraken-flashfiles-eng.robban.zip .";
    "copy_build_server_sem"="scp -r robban@10.239.124.56:/home/robban/sem/out/target/product/gtt_abl_car/fast_flashfiles/ .";
    "connect_serial0"="sudo minicom -b 115200 -D /dev/ttyUSB0";
    "connect_serial1"="sudo minicom -b 115200 -D /dev/ttyUSB1";
    "mount_bs"="sshfs -o reconnect,ServerAliveInterval=15,ServerAliveCountMax=3 robban@10.239.124.56:/ /home/robban/buildserver/";
    "mount_audio_bs"="sshfs -o reconnect,cache=yes,kernel_cache,cipher=arcfour,compression=no,ServerAliveInterval=15,ServerAliveCountMax=3 robban@10.239.126.22:/home/robban/ /home/robban/bs/";
    "mount_ihu"="sudo sshfs -o reconnect,cache=yes,kernel_cache,cipher=arcfour,compression=no,ServerAliveInterval=15,ServerAliveCountMax=3,uid=1000,gid=100,allow_other robban@10.239.126.22:/home/robban/ihu/ /home/robban/ihu/";
    "mount_sem"="sudo sshfs -o reconnect,cache=yes,kernel_cache,cipher=arcfour,compression=no,ServerAliveInterval=15,ServerAliveCountMax=3,uid=1000,gid=100,allow_other robban@10.239.126.22:/home/robban/sem/ /home/robban/sem/";
    "mount_sdrive"="sudo mount -t cifs //segot-fp02.europe.delphiauto.net/groupe01$ ~/S -o rw,auto,nounix,dir_mode=0777,file_mode=0777,user=mj08y1";
    "unmount_bs"="fusermount3 -u /home/robban/buildserver/";
    "create_tags" = "/home/robban/dotfiles/scripts/create_tags.sh";
    "sem" = "export ANDROID_BUILD_TOP=/ext/sem";
    "ihu" = "export ANDROID_BUILD_TOP=/ext/ihu";
    "caudio" = "cd $ANDROID_BUILD_TOP/vendor/aptiv/components/audio";
    "croot" = "cd $ANDROID_BUILD_TOP";
    "bur" = "cmake -Bbuild_ut -H. -DBUILD_UT_FOR_HOST=1 && cmake --build build_ut --target all -- -j 6 && ./build_ut/ut/amplifier_hisip_test";
    "audio-build" = "nix-shell --pure /home/robban/dotfiles/modules/shell/";
    "plantuml" = "java -jar /home/robban/.config/plantuml/plantuml.jar -tpng";
#    "compile_commands_convert_sem" = "sed -i -e 's|/home/robban/sem/|/ext/sem/|g'";
#    "compile_commands_convert_ihu" = "sed -i -e 's|/home/robban/ihu/|/ext/ihu/|g'";
#    "enable_debuglog" ='"find . -type f -exec sed -i 's|//#define LOG_NDEBUG 0|#define LOG_NDEBUG 0|g' {} +";
  };

  environment.etc."inputrc".text = lib.mkForce (
    builtins.readFile <nixpkgs/nixos/modules/programs/bash/inputrc>
    + ''
      #  alternate mappings for "page up" and "page down" to search the history
      "\e[5~": history-search-backward
      "\e[6~": history-search-forward
      "\eOc": forward-word
      "\eOd": backward-word
    ''
  );

   environment.etc."fuse.conf".text = ''
      user_allow_other
      mount_max = 1000
  '';

  networking.networkmanager.enable = true;

  # Define a user account.
  security.sudo.extraConfig = ''
  %wheel      ALL=(ALL:ALL) NOPASSWD: ${config.system.build.nixos-rebuild}/bin/nixos-rebuild switch
  %wheel      ALL=(ALL:ALL) NOPASSWD: ${pkgs.systemd}/bin/systemctl restart bluetooth.service
  %wheel      ALL=(ALL:ALL) NOPASSWD: ${pkgs.minicom}/bin/minicom -b 115200 -D /dev/ttyUSB0
  %wheel      ALL=(ALL:ALL) NOPASSWD: ${pkgs.minicom}/bin/minicom -b 115200 -D /dev/ttyUSB1
'';

   users.extraUsers.robban = {
     isNormalUser = true;
     extraGroups = ["wheel" "docker" "sound" "pulse" "audio" "networkmanager" "plugdev" "fuse" "mlocate"];
     uid = 1000;
  };

  # Enable cron service
  services.cron = {
    enable = true;
    systemCronJobs = [
      "0 */3 * * *        root    updatedb --output=/home/robban/.config/locatedb/rootdb"
      "15 */3 * * *     robban  /home/robban/dev/scripts/update_ext_db.sh"
      "45 9 * * *     robban  /home/robban/dev/scripts/create_tags.sh"
      "15 * * * *       robban  rclone copy remote:passwords.kdbx /home/robban/Downloads/dropbox/"
      "20 * * * *       robban  /home/robban/dev/scripts/backup.sh"
    ];
  };


services.openssh = {
    enable = true;
};

#RUN+="${pkgs.bash}
  # Android usb rules 2a70:f003
  services.udev.extraRules = ''
    # IHU 
    SUBSYSTEM=="usb", ATTR{idVendor}=="8087", MODE="0666", GROUP="plugdev"
    SUBSYSTEM=="usb", ATTR{idVendor}=="0fce", MODE="0666", GROUP="plugdev"
    SUBSYSTEM=="usb", ATTR{idVendor}=="18d1", MODE="0666", GROUP="plugdev" 
    # OnePlus
    SUBSYSTEM=="usb", ATTR{idVendor}=="2a70" MODE=”0666", GROUP="plugdev"
    #SUBSYSTEM=="usb", ATTR{idVendor}=="2a70", ATTR{idProduct}=="4ee7", MODE=”0666", GROUP="plugdev"
    #nvidia shield
    SUBSYSTEM=="usb", ATTR{idVendor}=="0955", ATTR{idProduct}=="cf05", MODE="0666", GROUP="plugdev"
    # Thunderbolt udev rules for ACL (device auto approval)
    SUBSYSTEM=="thunderbolt" ENV{DEVTYPE}=="thunderbolt_device" ACTION=="add"    ATTR{authorized}=="0" RUN+="${pkgs.thunderbolt}/bin/tbtacl add    $devpath"
    SUBSYSTEM=="thunderbolt" ENV{DEVTYPE}=="thunderbolt_device" ACTION=="change" ATTR{authorized}!="0" RUN+="${pkgs.thunderbolt}/bin/tbtacl change $devpath"
'';
    
  nixpkgs.config.allowUnfree = true;
  nixpkgs.config.firefox.enableAdobeFlash = true;
  nixpkgs.config.firefox.enableAdobeFlashDRM = true;
  nixpkgs.config.chromium.enablePepperFlash = true; # Chromium's non-NSAPI alternative to Adobe Flash
  #nixpkgs.config.chromium.enableWideVine = true;
  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.autoUpgrade.enable = true;
  system.stateVersion = "18.03"; # Did you read the comment?



}
